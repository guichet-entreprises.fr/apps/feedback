# Changelog

Ce fichier contient les modifications techniques du module.

- Projet : [Feedback](https://tools.projet-ge.fr/gitlab/minecraft/ge-feedback)



## [2.12.4.0] - 2019-11-25

-Ajout des CSP

| Attribut CSP   |Valeur à renseigner / ajouter |
|-----------|:-------------:|
| script-src | https://{VM_FEEDBACK}|
| style-src | https://{VM_FEEDBACK}|
| img-src | https://{VM_FEEDBACK}|
| connect-src | https://{VM_FEEDBACK}|


- Nouveau fichier _Changelog.md_


