package fr.ge.feedback.webclient.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.web.search.bean.DatatableSearchQuery;
import fr.ge.common.utils.web.search.bean.DatatableSearchResult;

/**
 * Abstract controller used for mapping search page and manage jQuery datatables
 * AJAX search.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public abstract class AbstractSearchController<T> {

    /**
     * Search page mapping.
     *
     * @return search page template name (ie "search/main"), prefixed by
     *         {@link #templatePrefix()}
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String search() {
        return this.templatePrefix() + "/search/main";
    }

    /**
     * Get search filters.
     *
     * @return the filters
     */
    protected List<SearchQueryFilter> getDefaultSearchFilters() {
        return new ArrayList<>();
    }

    /**
     * jQuery datatable search mapping.
     *
     * @param criteria
     *            jQuery datatable search criteria
     * @param filters
     *            the filters
     * @return search result object
     */
    @RequestMapping(value = "/search/data", method = RequestMethod.GET)
    @ResponseBody
    public DatatableSearchResult<T> searchData(final DatatableSearchQuery criteria, final String[] filters) {
        final DatatableSearchResult<T> datatableSearchResult = new DatatableSearchResult<>(criteria.getDraw());

        final SearchQuery query = new SearchQuery(criteria.getStart(), criteria.getLength());

        if (null != criteria.getOrder()) {
            final List<SearchQueryOrder> orders = criteria.getOrder().stream() //
                    .map(src -> new SearchQueryOrder(criteria.getColumns().get(src.getColumn()).getData(), src.getDir())) //
                    .collect(Collectors.toList());

            query.setOrders(orders);
        }

        final List<SearchQueryFilter> defaultFilters = this.getDefaultSearchFilters();
        if (null != defaultFilters) {
            query.setFilters(defaultFilters);
        }

        if (null != filters) {
            Arrays.stream(filters).forEach(query::addFilter);
        }

        final SearchResult<T> searchResult = this.search(query);
        datatableSearchResult.setRecordsFiltered((int) searchResult.getTotalResults());
        datatableSearchResult.setRecordsTotal((int) searchResult.getTotalResults());

        if (searchResult.getContent() == null) {
            datatableSearchResult.setData(Collections.<T>emptyList());
        } else {
            datatableSearchResult.setData(searchResult.getContent());
        }

        return datatableSearchResult;
    }

    /**
     * Specify template prefix for search main page.
     *
     * @return template prefix
     */
    protected abstract String templatePrefix();

    /**
     * Search.
     *
     * @param query
     *            the query
     * @return search result
     */
    protected abstract SearchResult<T> search(SearchQuery query);

}
