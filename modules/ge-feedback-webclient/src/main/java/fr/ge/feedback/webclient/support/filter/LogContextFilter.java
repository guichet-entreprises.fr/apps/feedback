package fr.ge.feedback.webclient.support.filter;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.MDC;
import org.springframework.web.filter.GenericFilterBean;

/**
 * Filter to add information in all the logs. A map of information is added into
 * the logging context.
 *
 * @author Christian Cougourdan
 */
public class LogContextFilter extends GenericFilterBean {

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        try {
            MDC.put("correlationId", UUID.randomUUID().toString());
            chain.doFilter(request, response);
        } finally {
            MDC.clear();
        }
    }

}
