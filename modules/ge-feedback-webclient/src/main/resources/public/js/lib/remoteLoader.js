require([ 'jquery', 'lib/loader' ], function($, Loader) {

    function initialize() {
        var remotes = $('*[data-remote]');

        if (remotes.size() > 0) {
            Loader.show();
        }

        remotes.each(function(idx, elm) {
            var $this = $(elm), url = $this.data('remote');
            $.ajax(url, {
                dataType : 'html'
            }).done(function(html) {
                $this.html(html);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                $this.html(jqXHR.responseText);
            }).always(function(jqXHR, textStatus) {
                Loader.hide();
            });
        });
    }

    $(function() {
        initialize();
    });

    return {
        initialize: initialize
    };

});
