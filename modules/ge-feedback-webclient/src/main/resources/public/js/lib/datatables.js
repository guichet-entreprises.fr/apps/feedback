define([ 'jquery', 'datatables.net', 'lib/template', 'lib/func', 'lib/springFormParam' ], function($, DataTable, Template, func, sfp) {

    var defaultOptions = {
        retrieve : true,
        autoWidth : false,
        language : {
            info : 'Affichage de _START_ &agrave; _END_ pour un total de _TOTAL_ entr&eacute;es',
            infoEmpty : 'Aucune entr&eacute;e &agrave; afficher',
            loadingRecords : 'Chargement ...',
            paginate : {
                first : '<i class="fa fa-angle-double-left"></i>',
                previous : '<i class="fa fa-angle-left"></i>',
                next : '<i class="fa fa-angle-right"></i>',
                last : '<i class="fa fa-angle-double-right"></i>'
            },
            processing : 'Chargement ...',
            zeroRecords : 'Aucune entr&eacute;e trouv&eacute;e'
        }
    };

    function linkRenderer(link, target) {
        var tpl = func.renderer('<a href="' + link + '"' + (target ? ' target="' + target + '"' : '') + '>{{__data__}}</a>');
        return function(data, type, row) {
            return tpl($.extend({
                __data__ : data
            }, row));
        };
    }

    function templateRenderer(templateName) {
        var tpl = Template.find(templateName);
        return function(data, type, row) {
            return tpl.render(row);
        };
    }

    function errorRenderer(data, type, row) {
        return '&lt;No renderer&gt;';
    }

    var oldDataTable = $.fn.DataTable;

    $.fn.DataTable = function(opts) {
        var newOpts = $.extend({}, defaultOptions, opts || {});

        return oldDataTable.call($(this).each(function() {
            var tbl = $(this), ajax = tbl.data('ajax'), opts = newOpts;
            if (!oldDataTable.isDataTable($(this))) {
                if (ajax) {
                    if ($.isFunction(window[ajax])) {
                        opts = $.extend(true, {}, opts, {
                            serverSide : true,
                            ajax : window[ajax]
                        });
                        tbl.removeAttr('data-ajax').removeData('ajax');
                    } else {
                        opts = $.extend(true, {}, opts, {
                            serverSide : true,
                            ajax : {
                                url : ajax,
                                type : 'GET',
                                dataType : 'json'
                            }
                        });
                    }
                }

                if (tbl.data('order')) {
                    opts.order = JSON.parse(tbl.data('order').replace(/'/g, '"'));
                    tbl.removeAttr('data-order').removeData('order');
                }

                $('thead th', tbl).each(function(idx, elem) {
                    var th = $(elem), link = th.data('link'), render = th.data('render'), templateName = th.data('template');
                    th.data('render', null).removeAttr('data-render');
                    th.data('template', null).removeAttr('data-template');
                    if (templateName) {
                        th.data('render', templateRenderer(templateName));
                    } else if (render) {
                        th.data('render', func[render] || window[render] || errorRenderer);
                    } else if (link) {
                        th.data('render', linkRenderer(link, th.data('target')));
                    }
                });
            }
            oldDataTable.call(tbl, opts);
        }));
    }

    return $.fn.DataTable;

});
