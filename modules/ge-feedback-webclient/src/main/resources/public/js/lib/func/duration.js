/*!
 * Depends:
 *      lib/func/translate.js
 */
define([ 'lib/i18n' ], function(i18n) {

    var TWICE = 2;
    var DURATION_MINUTE = 60;
    var DURATION_HOUR = 60 * DURATION_MINUTE;
    var DURATION_DAY = 24 * DURATION_HOUR;
    var DURATION_MONTH = 30 * DURATION_DAY;
    var DURATION_YEAR = 365 * DURATION_DAY;

    return function(value) {
        var duration = (new Date().getTime() - value) / 1000;

        if (duration <= 1) {
            return i18n('a moment ago');
        } else if (duration < DURATION_MINUTE) {
            return i18n('{0} secondes', Math.ceil(duration));
        } else if (duration < TWICE * DURATION_MINUTE) {
            return i18n('a minute');
        } else if (duration < DURATION_HOUR) {
            return i18n('{0} minutes', Math.ceil(duration / DURATION_MINUTE));
        } else if (duration < TWICE * DURATION_HOUR) {
            return i18n('an hour');
        } else if (duration < DURATION_DAY) {
            return i18n('{0} hours', Math.ceil(duration / DURATION_HOUR));
        } else if (duration < TWICE * DURATION_DAY) {
            return i18n('a day');
        } else if (duration < DURATION_MONTH) {
            return i18n('{0} days', Math.ceil(duration / DURATION_DAY));
        } else if (duration < TWICE * DURATION_MONTH) {
            return i18n('a month');
        } else if (duration < DURATION_YEAR) {
            return i18n('{0} month', Math.ceil(duration / DURATION_MONTH));
        } else if (duration < TWICE * DURATION_YEAR) {
            return i18n('a year');
        } else {
            return i18n('{0} years', Math.ceil(duration / DURATION_YEAR));
        }
    };

});
