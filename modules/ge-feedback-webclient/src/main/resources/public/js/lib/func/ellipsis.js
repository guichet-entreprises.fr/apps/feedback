define([], function() {

	return function (data) {
		if(data.length < 20){
			return data; 
		}else{
			return data.substr( 0, 20 ) + '...';
		}
	};


});
