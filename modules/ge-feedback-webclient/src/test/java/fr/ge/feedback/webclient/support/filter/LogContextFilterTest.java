package fr.ge.feedback.webclient.support.filter;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.MDC;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

/**
 * Class LogContextFilterTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(MockitoJUnitRunner.class)
public class LogContextFilterTest {

    /**
     * Test simple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSimple() throws Exception {
        final LogContextFilter filter = new LogContextFilter();
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final FilterChain chain = mock(FilterChain.class);

        final Map<String, String> actual = new HashMap<>();

        doAnswer(invocation -> {
            final Map<String, String> context = MDC.getCopyOfContextMap();
            if (null != context) {
                actual.putAll(context);
            }
            return null;
        }).when(chain).doFilter(request, response);

        filter.doFilter(request, response, chain);

        assertThat(actual.get("correlationId"), notNullValue());
    }

    /**
     * Test clean.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testClean() throws Exception {
        final LogContextFilter filter = new LogContextFilter();
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final FilterChain chain = mock(FilterChain.class);

        filter.doFilter(request, response, chain);

        assertTrue(MDC.getCopyOfContextMap().isEmpty());
    }

    /**
     * Test error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testError() throws Exception {
        final LogContextFilter filter = new LogContextFilter();
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final FilterChain chain = mock(FilterChain.class);

        final Map<String, String> actual = new HashMap<>();

        doAnswer(invocation -> {
            final Map<String, String> context = MDC.getCopyOfContextMap();
            if (null != context) {
                actual.putAll(context);
            }
            throw new RuntimeException();
        }).when(chain).doFilter(request, response);

        try {
            filter.doFilter(request, response, chain);
            fail("RuntimeException expected");
        } catch (final RuntimeException ex) {
            // Nothing to do
        }

        assertFalse(actual.isEmpty());
        assertTrue(MDC.getCopyOfContextMap().isEmpty());
    }

}
