'use strict';



function extend() {
    var sources = Array.from(arguments);
    var dst = {}, src;

    if (1 === sources.length && Array.isArray(sources[0])) {
        sources = sources[0];
    }

    while (src = sources.shift()) {
        Object.keys(src).forEach(function (key) {
            dst[key] = src[key];
        });
    }
    return dst;
}



module.exports = function (grunt) {

    var async = require('async');
    var chalk = require('chalk');
    var path = require('path');

    grunt.registerMultiTask('template', 'Process HTML templates', function() {
        var engine = require('ejs');

        var opts = this.options({ context: {} });
        var globalContext = opts.context;

        var done = this.async();
        var cnt = 0;

        if (this.files.length < 1) {
            grunt.log.error('No source files were provided.');
        }

        async.eachSeries(this.files, function (elm, next) {
            var engine = require('ejs');

            grunt.log.debug('Processing ' + JSON.stringify(elm.src) + ' to "' + elm.dest + '" (' + elm.src.length + ')');
            if (elm.src.length < 1) {
                grunt.log.error('No source files were provided.');
            }

            grunt.log.debug('Render HTML template ...');
            engine.renderFile(elm.src[0], globalContext, undefined, function (err, str) {
                if (undefined === str) {
                    grunt.log.error('Error while rendering HTML template : ' + err);
                } else {
                    grunt.file.write(elm.dest, str);
                }
                cnt += 1;
                return next();
            }); //
        }, function () {
            grunt.log.ok(chalk.cyan(cnt) + ' ' + grunt.util.pluralize(cnt, 'file/files') + ' processed');
            done();
        });
    });

};
