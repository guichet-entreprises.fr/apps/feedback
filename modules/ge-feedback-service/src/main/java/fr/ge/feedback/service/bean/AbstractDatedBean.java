package fr.ge.feedback.service.bean;

import java.util.Date;
import java.util.Optional;

/**
 * The Class AbstractDatedBean.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public abstract class AbstractDatedBean<T extends AbstractDatedBean<T>> {

    /** The id. */
    private Long id;

    /** The created. */
    private Date created;

    /** The updated. */
    private Date updated;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public T setId(final Long id) {
        this.id = id;
        return (T) this;
    }

    /**
     * Gets the created.
     *
     * @return the created
     */
    public Date getCreated() {
        return this.created;
    }

    /**
     * Sets the created.
     *
     * @param created
     *            the new created
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public T setCreated(final Date created) {
        this.created = Optional.ofNullable(created).map(value -> (Date) value.clone()).orElse(null);
        return (T) this;
    }

    /**
     * Gets the updated.
     *
     * @return the updated
     */
    public Date getUpdated() {
        return this.updated;
    }

    /**
     * Sets the updated.
     *
     * @param updated
     *            the new updated
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public T setUpdated(final Date updated) {
        this.updated = Optional.ofNullable(updated).map(value -> (Date) value.clone()).orElse(null);
        return (T) this;
    }

}
