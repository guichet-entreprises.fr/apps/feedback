const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

var project = {
        build: {
            directory: 'target/classes',
            outputDirectory: './target/classes/public',
            sourceDirectory: './src/main/resources/public'
        }
};



module.exports = {
    mode: 'production',
    entry: project.build.sourceDirectory + '/js/widget.js',
    output: {
        filename: 'widget.js',
        path: path.resolve(__dirname, project.build.outputDirectory + '/js'),
        publicPath: '${ws.feedback.public.url}/public/v1/feedback/'
    },
    module: {
        rules: [
            {
                test: /\.(html)$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            minimize: true
                        }
                    }
                ]
            },
            {
                test: /\.less$/,
                use: [ {
                    loader: 'style-loader',
                    options: {
                        injectType: 'linkTag'
                    }
                }, {
                    loader: 'file-loader',
                    options: {
                        name: '[contenthash].css'
                    }
                }, 'less-loader' ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [ {
                    loader: 'url-loader',
                    options: {
                        limit: -1
                    }
                } ]
            }
        ]
    },
    plugins: [
//        new MiniCssExtractPlugin({
//            filename: '[name].[hash].css',
//            chunkFilename: '[id].[hash].css'
//        }),
    ],
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                extractComments: true
            })
        ]
    }
};
