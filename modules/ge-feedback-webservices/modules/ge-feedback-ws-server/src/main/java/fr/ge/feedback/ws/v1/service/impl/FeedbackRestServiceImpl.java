/**
 *
 */
package fr.ge.feedback.ws.v1.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.map.LRUMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.feedback.service.IFeedbackService;
import fr.ge.feedback.service.bean.FeedbackBean;
import fr.ge.feedback.ws.util.CoreUtil;
import fr.ge.feedback.ws.v1.service.IFeedbackRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

/**
 * @author bsadil
 *
 */
@Api("Feedback - Public Rest Services")
@Path("/public/v1/feedback")
public class FeedbackRestServiceImpl implements IFeedbackRestService {

    private static final int MAX_TEXT_SIZE = 254;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackRestServiceImpl.class);

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("[$][{]([^}]+)[}]");

    private String widgetScriptResource;

    @Autowired
    private Properties appProperties;

    @Autowired
    private IFeedbackService feedbackService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response createFeedBack(@ApiParam("User feedback  for specific page ") final String comment, @ApiParam("URI of page ") final String page, @ApiParam("User evaluation") final Long rate) {
        final FeedbackBean feedback = new FeedbackBean();
        feedback.setComment(StringUtils.substring(comment, 0, MAX_TEXT_SIZE));
        feedback.setPage(StringUtils.substring(page, 0, MAX_TEXT_SIZE));
        feedback.setRate(rate);
        feedback.setCreated(new Date());
        feedback.setUpdated(new Date());

        this.feedbackService.create(feedback);

        return Response.ok(feedback.getId()).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response updateFeedBack(@ApiParam("User feedback  for specific page ") final String comment, @ApiParam("URI of page ") final String page, @ApiParam("User evaluation") final Long rate,
            @ApiParam("id of feedback") final Long id) {

        final FeedbackBean feedback = new FeedbackBean();
        feedback.setId(id);
        feedback.setComment(StringUtils.substring(comment, 0, MAX_TEXT_SIZE));
        feedback.setPage(StringUtils.substring(page, 0, MAX_TEXT_SIZE));
        feedback.setRate(rate);
        feedback.setUpdated(new Date());

        this.feedbackService.update(feedback);

        return Response.ok(feedback.getId()).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response widget(final String resourcePath) {

        String mediaType;
        if (resourcePath.endsWith(".js")) {
            mediaType = "text/javascript";
        } else if (resourcePath.endsWith(".png")) {
            mediaType = "image/png";
        } else if (resourcePath.endsWith(".css")) {
            mediaType = "text/css";
        } else {
            mediaType = "application/octet-stream";
        }

        final byte[] resource = this.getResourceFromCache(resourcePath);
        if (null == resource) {
            return Response.noContent().build();
        } else {
            return Response.ok(resource, mediaType).build();
        }
    }

    private final Map<String, byte[]> resourceCache = new LRUMap<>();

    private byte[] getResourceFromCache(final String resourcePath) {
        if (!this.resourceCache.containsKey(resourcePath) || true) {
            try (final InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("public/js/" + resourcePath)) {
                if (resourcePath.endsWith(".js")) {
                    final String script = new String(IOUtils.toByteArray(in), StandardCharsets.UTF_8);
                    String translatedScript = CoreUtil.searchAndReplace(script, PLACEHOLDER_PATTERN, m -> {
                        final String key = m.group(1);
                        final String value = this.appProperties.getProperty(key, m.group());
                        return value.replace("$", "\\$");
                    });
                    this.resourceCache.put(resourcePath, translatedScript.getBytes(StandardCharsets.UTF_8));
                } else {
                    this.resourceCache.put(resourcePath, IOUtils.toByteArray(in));
                }
            } catch (final IOException ex) {
                LOGGER.warn("Unable to read widget script", ex);
            }
        }

        return this.resourceCache.get(resourcePath);
    }

}
